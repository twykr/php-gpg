<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PHP GPG Class 
 *
 * This PHP class takes any number of PGP or GPG keys that are available via a 
 * public GPG keyring on a webserver, and encrypts the submitted block of
 * information to those keys.  There is error checking to make sure that the
 * submitted keys are valid.
 *
 * PHP version 4
 *
 * @category   Exact Target
 * @package    Subscription Handler
 * @author     Nathan Ho <webmaster@theoslogic.com>
 * @copyright  (c) 2005 Nathan Ho
 * @version    1.1
 *
 * USAGE:
 *
 * $yourVar = new gpg();
 *
 * --------------------------
 * PROPERTY DECLARATIONS
 * --------------------------
 * $yourVar->keyring        // this var holds the location of your GPG keyring.
 *                          // and should ONLY hold public PGP/GPG keys
 * $yourVar->keys[]         // can be single var or array; holds pgp/gpg key(s)
 * $yourVar->data           // holds the block of data to be encrypted
 * 
 * --------------------------
 * PROPERTY DECLARATIONS
 * --------------------------
 * $yourVar->autolocate_gpg();      // try and automatically locate the GPG binary
 * $encrypted = $yourVar->gpgit();  // returns the encrypted block of data
 *
 *
**/

class gpg 
{

    /*------{ PROPERTY DECLARATIONS }-------------------------------------*/

    // holds the location of the GPG keyring files
    // default value is "{DOCUMENT_ROOT}/.gnupg"
    var $keyring;

    // holds the GPG binary path
    var $gpgbin     = "/usr/bin/gpg";

    // sets up the command to validate the GPG / PGP keys
    var $gpgck;

    // sets up the command for encrypting the data to the valid keys
    var $gpgcmd;

    // holds a temporary file name for the encrypted data
    var $tmpfile;

    // this holds the PGP/GPG key(s) with which we will be encrypting the data
    var $keys;

    // holds the data to be encrypted
    var $data;


    /*------{ METHOD DECLARATIONS }---------------------------------------*/

    function gpgit() 
    {
        // first, validate the submitted key(s)
        $this->validate_keys();

        // okay - we have valid keys.  Let's encrypt the contents now
        $handle = popen($this->gpgcmd, "w");
        if (!$handle) { 
            die("Failure in creating encrypted content - please contact the site administrator! (5n)");
        }
        // we only write the unencrypted data directly to the GPG process, and not to a file 
        fwrite($handle,$this->data);
        pclose($handle);
        $encrypted = file_get_contents($this->tmpfile);
        unlink($this->tmpfile);

        return $encrypted;
    }

    function validate_keys()
    {
        // first, check to see if we're dealing with one PGP key, or multiple keys
        if (is_array($this->keys)) {
         // prep a variable to let us know if they've passed a valid key or not
         unset($valid_keys);
         foreach ($this->keys as $addkey) {
           // now we're going to make sure that it's just a key, and nothing else malicious
           // if it's an email address, validate that
           if (strstr($addkey, "@")) {
             $this->validate_email($addkey);
           }
           // if it's not an email address, watch for improper characters
           if ((strstr($addkey,";")) || (strstr($addkey,"\n")) || (strstr($addkey,"\r"))) {
             die("Input error - please check the submitted data (10n)");
           }
           // check to see if this is a valid PGP key
           $check_this = shell_exec($this->gpgck.$addkey);
           if (strstr($check_this, "pub")) {
             $this->gpgcmd .= " -r ".$addkey;
             // a valid key has been submitted!
             $valid_keys = "yes";
           } else {
             if (!$valid_keys) {
               // still no valid key yet
               $valid_keys = "no";
             }
           }
         }
         if ($valid_keys == "no") {
           // no valid PGP keys were submitted - die gracefully
           die("No valid PGP keys submitted for encryption. (15n)");
         }
        } else {
          // only one key was submitted - let's check that key
          $check_this = shell_exec($this->gpgck.$this->keys);
          if (strstr($check_this, "pub")) {
            // this key is valid - let's use it!
            $this->gpgcmd .= " -r ".$this->keys;
            // nope, not a valid key - die gracefully
          } else die("No valid PGP keys submitted for encryption. (20n)");
        }
    }
    
    // this function exists to do basic validation on a submitted email address key
    // just to make sure that there's no malicious data involved
    function validate_email($email)
    {
        if ((strstr($email,";")) || (strstr($email,"\n")) || (strstr($email,"\r"))) {
            die("Input error - please check the submitted data (25n)");
        }
        list($local, $domain) = explode("@", $email);

        $pattern_local = '^([0-9a-z]*([-|_]?[0-9a-z]+)*)(([-|_]?)\.([-|_]?)[0-9a-z]*([-|_]?[0-9a-z]+)+)*([-|_]?)$';
        $pattern_domain = '^([0-9a-z]+([-]?[0-9a-z]+)*)(([-]?)\.([-]?)[0-9a-z]*([-]?[0-9a-z]+)+)*\.[a-z]{2,4}$';

        $match_local = eregi($pattern_local, $local);
        $match_domain = eregi($pattern_domain, $domain);

        if ((!$match_local) || (!$match_domain)) {
            die("Input error - please check the submitted data (30n)");
        }
    }

    // modify whether or not we should attempt to auto-detect the GPG binary
    function autolocate_gpg()
    {
        // try and auto-detect the GPG binary, if enabled
        $this->gpgbin    = escapeshellcmd(shell_exec("which gpg"));
        // force the discovered path to be text
        settype($this->gpgbin, "string");

        // now reset the command calls appropriately
        if ($this->gpgbin) {
            // for validating submitted PGP / GPG key(s)
            $this->gpgck     = $this->gpgbin." --no-secmem-warning --homedir ".$this->keyring." --list-keys ";

            // for encrypting the data block to the submitted PGP / GPG key(s)
            $this->gpgcmd    = $this->gpgbin." -a --always-trust --batch --no-secmem-warning --homedir ".$this->keyring." -e -o ".$this->tmpfile;
        } else {
            die("I'm sorry, but the GPG binary could not be found on this server.  Please specify the location manually! (40n)\n");
        }
    }

    // constructor function
    function gpg()
    {

        // set the directory for the GPG keys
        $this->keyring = $_SERVER['DOCUMENT_ROOT']."/.gnupg";
        
        // temporary file name - will only hold encrypted data
        $this->tmpfile   = substr(md5(microtime()), 7, 10).".asc";

        // for validating submitted PGP / GPG key(s)
        $this->gpgck     = $this->gpgbin." --no-secmem-warning --homedir ".$this->keyring." --list-keys ";

        // for encrypting the data block to the submitted PGP / GPG key(s)
        $this->gpgcmd    = $this->gpgbin." -a --always-trust --batch --no-secmem-warning --homedir ".$this->keyring." -e -o ".$this->tmpfile;

    }

// end of gpg class

}

?>
