<?php

/***************************************************************************
// PHP function for GPG encryption  v0.4
// Author: Nathan Ho (webmaster@theoslogic.com) | TheosLogic Productions
// http://www.theoslogic.com/scripts/secure/
//
// Right are hereby granted for you to use this script however you want.
// It would be nice if you left the above lines intact :)
//
//  Usage:   $encrypted = gpgit($data, $pgpkeys); 
//
// This function takes two arguments:
//  (1)  Data ($data) to be encrypted
//  (2)  a single variable or array ($pgpkeys) of PGP keys/recipient(s)
//       and returns the encrypted text
***************************************************************************/

function gpgit($data, $pgpkeys) {

  // set some variables so that the script will run
  $gpgdir    = $_SERVER['DOCUMENT_ROOT']."/.gnupg";
  $gpgbin    = escapeshellcmd(shell_exec("which gpg"));
  // if the above auto-detection fails, try using this pre-set path
  //$gpgbin    = "/usr/bin/gpg";
  $gpgck     = $gpgbin." --no-secmem-warning --homedir ".$gpgdir." --list-keys ";
  $gpgcmd    = $gpgbin." -a --always-trust --batch --no-secmem-warning --homedir ".$gpgdir." -e ";
  $errLog = "./errors.log";  // path to the error log file for the proc_open function

  // first, check to see if we're dealing with one PGP key, or multiple keys
  if (is_array($pgpkeys)) {
   // prep a variable to let us know if they've passed a valid key or not
   unset($valid_keys);
   foreach ($pgpkeys as $addkey) {
     // now we're going to make sure that it's just a key, and nothing else malicious
     // if it's not an email address, watch for improper characters
     if ((strstr($addkey,";")) || (strstr($addkey,"\n")) || (strstr($addkey,"\r"))) {
       die("Input error - please check the submitted data (10n)");
     }
     // if it's an email address, validate that
     if (strstr("@", $addkey)) {
       $this->validate_email($addkey);
     }
     // check to see if this is a valid PGP key
     $check_this = shell_exec($gpgck.$addkey);
     if (strstr($check_this, "pub")) {
       $gpgcmd .= " -r ".$addkey;
       // a valid key has been submitted!
       $valid_keys = "yes";
     } else {
       if (!$valid_keys) {
         // still no valid key yet
         $valid_keys = "no";
       }
     }
   }
   if ($valid_keys == "no") {
     // no valid PGP keys were submitted - die gracefully
     die("No valid PGP keys submitted for encryption. (20n)");
   }
  } else {
    // only one key was submitted - let's check that key
    $check_this = shell_exec($gpgck.$pgpkeys);
    if (strstr($check_this, "pub")) {
      // this key is valid - let's use it!
      $gpgcmd .= " -r ".$pgpkeys;
      // nope, not a valid key - die gracefully
    } else die("No valid PGP keys submitted for encryption. (30n)");
  }

  // okay - we have valid keys.  Let's encrypt the contents now
  $dspecs = array(
      0=>array("pipe", "r"),
      1=>array("pipe", "w"),
      2=>array("file", $errLog, "a")
  );

  unset ($encrypted, $procdata);
  $gpgproc = proc_open($gpgcmd, $dspecs, $pipes);

  if (is_resource($gpgproc)) {
      fwrite($pipes[0], $data);
      fclose($pipes[0]);

      while($procdata = fgets($pipes[1], 1024)) {
          $encrypted .= $procdata;
      }
      fclose($pipes[1]);
  }
 
  return $encrypted;
}

// this function exists to do basic validation on a submitted email address key
// just to make sure that there's no malicious data involved
function validate_email($email)
{
    $pattern_local = '^([0-9a-z]*([-|_]?[0-9a-z]+)*)(([-|_]?)\.([-|_]?)[0-9a-z]*([-|_]?[0-9a-z]+)+)*([-|_]?)$';
    $pattern_domain = '^([0-9a-z]+([-]?[0-9a-z]+)*)(([-]?)\.([-]?)[0-9a-z]*([-]?[0-9a-z]+)+)*\.[a-z]{2,4}$';

    $match_local = eregi($pattern_local, $local);
    $match_domain = eregi($pattern_domain, $domain);

    if ((!$match_local) || (!$match_domain)) {
        die("Input error - please check the submitted data (40n)");
    }
}

?>
