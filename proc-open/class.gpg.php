<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PHP GPG Class 
 *
 * This PHP class takes any number of PGP or GPG keys that are available via a 
 * public GPG keyring on a webserver, and encrypts the submitted block of
 * information to those keys.  There is error checking to make sure that the
 * submitted keys are valid.
 *
 * PHP version 4
 *
 * @category   Exact Target
 * @package    Subscription Handler
 * @author     Nathan Ho <webmaster@theoslogic.com>
 * @copyright  (c) 2005 Nathan Ho
 * @version    1.1
 *
 * USAGE:
 *
 * $yourVar = new gpg();
 *
 * --------------------------
 * PROPERTY DECLARATIONS
 * --------------------------
 * $yourVar->keyring        // this var holds the location of your GPG keyring.
 *                          // and should ONLY hold public PGP/GPG keys
 * $yourVar->keys[]         // can be single var or array; holds pgp/gpg key(s)
 * $yourVar->data           // holds the block of data to be encrypted
 * 
 * --------------------------
 * METHOD DECLARATIONS
 * --------------------------
 * $yourVar->autolocate_gpg();      // try to automatically locate the GPG binary
 * $encrypted = $yourVar->gpgit();  // returns the encrypted block of data
 *
 *
**/

class gpg 
{

    /*------{ PROPERTY DECLARATIONS }-------------------------------------*/

    // holds the location of the GPG keyring files
    // default value is "{DOCUMENT_ROOT}/.gnupg"
    var $keyring;

    // holds the GPG binary path
    var $gpgbin     = "/usr/bin/gpg";

    // sets up the command to validate the GPG / PGP keys
    var $gpgck;

    // sets up the command for encrypting the data to the valid keys
    var $gpgcmd;

    // this holds the PGP/GPG key(s) with which we will be encrypting the data
    var $keys;

    // holds the data to be encrypted
    var $data;


    /*------{ METHOD DECLARATIONS }---------------------------------------*/

    function gpgit() 
    {
        // first, validate the submitted key(s) and add appropriate ones to the GPG command string
        $this->validate_keys();

        // select an error log, just in case...
        $errorLog = "./errors.log";
        
        // okay - we have valid keys.  Let's encrypt the contents now
        $dspecs = array(
            0=>array("pipe", "r"),
            1=>array("pipe", "w"),
            2=>array("file", $errorLog, "a")
        );

        unset ($encrypted, $procdata);
        $gpgproc = proc_open($this->gpgcmd, $dspecs, $pipes);

        // make sure that the process was started properly
        if (is_resource($gpgproc)) {
            // first we dump the data into the pipe
            fwrite($pipes[0], $this->data);  // insert the data
            fclose($pipes[0]);  // close the pipe

            // then we get the results out of the pipe, one row at a time
            while($procdata = fgets($pipes[1], 1024)) {
                $encrypted .= $procdata;
            }
            fclose($pipes[1]); // close the pipe
        } else {
            die("Error: no process available! (5n)\n");
        }
        return $encrypted;
    }

    function validate_keys()
    {
        // first, check to see if we're dealing with one PGP key, or multiple keys
        if (is_array($this->keys)) {
         // prep a variable to let us know if they've passed a valid key or not
         unset($valid_keys);
         foreach ($this->keys as $addkey) {
           // now we're going to make sure that it's just a key, and nothing else malicious
           // if it's not an email address, watch for improper characters
           if ((strstr($addkey,";")) || (strstr($addkey,"\n")) || (strstr($addkey,"\r"))) {
             die("Input error - please check the submitted data (10n)");
           }
           // if it's an email address, validate that
           if (strstr("@", $addkey)) {
             $this->validate_email($addkey);
           }
           // check to see if this is a valid PGP key
           $check_this = shell_exec($this->gpgck.$addkey);
           if (strstr($check_this, "pub")) {
             $this->gpgcmd .= " -r ".$addkey;
             // a valid key has been submitted!
             $valid_keys = "yes";
           } else {
             if (!$valid_keys) {
               // still no valid key yet
               $valid_keys = "no";
             }
           }
         }
         if ($valid_keys == "no") {
           // no valid PGP keys were submitted - die gracefully
           die("No valid PGP keys submitted for encryption. (15n)");
         }
        } else {
          // first, we're going to make sure that it's just a key, and nothing else malicious
          if ((strstr(";", $this->keys)) || (strstr("\n",$this->keys)) || (strstr("\r",$this->keys))) {
             die("Input error - please check the submitted data (12n)");
          }
          // only one key was submitted - let's check that key
          $check_this = shell_exec($this->gpgck.$this->keys);
          if (strstr($check_this, "pub")) {
            // this key is valid - let's use it!
            $this->gpgcmd .= " -r ".$this->keys;
            // nope, not a valid key - die gracefully
          } else die("No valid PGP keys submitted for encryption. (20n)");
        }
    }

    // this function exists to do basic validation on a submitted email address key
    // just to make sure that there's no malicious data involved
    function validate_email($email)
    {
        list($local, $domain) = explode("@", $email);
        $pattern_local = '^([0-9a-z]*([-|_]?[0-9a-z]+)*)(([-|_]?)\.([-|_]?)[0-9a-z]*([-|_]?[0-9a-z]+)+)*([-|_]?)$';
        $pattern_domain = '^([0-9a-z]+([-]?[0-9a-z]+)*)(([-]?)\.([-]?)[0-9a-z]*([-]?[0-9a-z]+)+)*\.[a-z]{2,4}$';

        $match_local = eregi($pattern_local, $local);
        $match_domain = eregi($pattern_domain, $domain);

        if ((!$match_local) || (!$match_domain)) {
            die("Input error - please check the submitted data (20n)");
        }
    }

    // modify whether or not we should attempt to auto-detect the GPG binary
    function autolocate_gpg()
    {
        // try and auto-detect the GPG binary, if enabled
        $this->gpgbin    = escapeshellcmd(trim(shell_exec("which gpg")));
        // force the discovered path to be text
        settype($this->gpgbin, "string");

        // now reset the command calls appropriately
        if ($this->gpgbin != "") {
            // for validating submitted PGP / GPG key(s)
            $this->gpgck     = $this->gpgbin." --no-secmem-warning --homedir ".$this->keyring." --list-keys ";

            // for encrypting the data block to the submitted PGP / GPG key(s)
            $this->gpgcmd    = $this->gpgbin." -a --always-trust --batch --no-secmem-warning --homedir ".$this->keyring." -e ";
        } else {
            die("I'm sorry, but the GPG binary could not be found on this server.  Please specify the location manually! (40n)\n");
        }
    }

    // constructor function
    function gpg()
    {

        // set the directory for the GPG keys
        $this->keyring = $_SERVER['DOCUMENT_ROOT']."/.gnupg";
        
        // for validating submitted PGP / GPG key(s)
        $this->gpgck     = $this->gpgbin." --no-secmem-warning --homedir ".$this->keyring." --list-keys ";

        // for encrypting the data block to the submitted PGP / GPG key(s)
        $this->gpgcmd    = $this->gpgbin." -a --always-trust --batch --no-secmem-warning --homedir ".$this->keyring." -e ";

    }

// end of gpg class

}

?>
